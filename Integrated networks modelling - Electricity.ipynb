{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Integrated networks modelling - Electricity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***&copy; 2023 Martínez Ceseña<sup>1</sup> and Mancarella<sup>2</sup> — <sup>1,2</sup>University of Manchester, UK, and <sup>2</sup>University of Melbourne, Australia***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is one of the documents in a series of jupyter notebooks which presents a Newton's based formulation to simulate integrated electricity, heating and gas networks under steady-state conditions. This particular notebook presents the formulation for the power system model. More information about the models and some of their applications can also be found in the following literature:\n",
    "\n",
    "1. X. Liu and P. Mancarella, \"[Modelling, assessment and Sankey diagrams of integrated electricity-heat-gas networks in multi-vector district energy systems](https://www.sciencedirect.com/science/article/pii/S0306261915010259)\" in Applied Energy, Vol. 167, pp. 136 - 352, 2016.\n",
    "\n",
    "1. E. A. Martínez Ceseña, E. Loukarakis, N. Good and P. Mancarella, \"[Integrated Electricity-Heat-Gas Systems: Techno-Economic Modeling, Optimization, and Application to Multienergy Districts](https://ieeexplore.ieee.org/document/9108286),\" in Proceedings of the IEEE, Vol. 108, pp. 1392 –1410, 2020.\n",
    "\n",
    "1. E. A. Martínez Ceseña, T. Capuder and P. Mancarella, “[Flexible distributed multi-energy generation system expansion planning under uncertainty](https://ieeexplore.ieee.org/document/7064771),” IEEE Transactions on Smart Grid, Vol. 7, pp. 348 –357, 2016.\n",
    "\n",
    "1. G. Chicco, S. Riaz, A. Mazza and P. Mancarella, \"[Flexibility From Distributed Multienergy Systems](https://ieeexplore.ieee.org/document/9082595),\" in Proceedings of the IEEE, Vol. 108, pp. 1496-1517, 2020.\n",
    "\n",
    "1. E. Corsetti, S. Riaz, M. Riello, P. Mancarella, “[Modelling and deploying multi-energy flexibility: The energy lattice framework](https://www.sciencedirect.com/science/article/pii/S2666792421000238)”, Advances in Applied Energy, Vol. 2, 2021."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## List of contents"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Power network](#Power-network)\n",
    "- [Newton's method applied to power systems](#Newton's-method-applied-to-power-systems)\n",
    "- [Coding Newton's method](#Coding-Newton's-method)\n",
    "  - [Jacobian matrix](#Jacobian-matrix)\n",
    "  - [Vector of differences](#Vector-of-differences)\n",
    "  - [Correction factors](#Correction-factors)\n",
    "  - [Solving the model](#Solving-the-model)\n",
    "- [Development as python tool](#Development-as-python-tool)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Before we begin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we begin, be aware that, to benefit the most from this notebook, you will need a basic understanding of: \n",
    "- [Newton's method](https://www.sciencedirect.com/topics/mathematics/newtons-method), which is the method used in this notebook.\n",
    "- [Python](https://www.python.org/), which is the language used in this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The notebook also requires some python functionalities, which should be imported as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import scipy.sparse as sp\n",
    "import cmath, math"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Power network"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us begin by defining the information that is needed to represent a power system. For that purpose, let us begin with an illustrative 3-bus system presented in Figure 1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Power_Network_3Bus.png](Figures/Power_Network_3Bus.png)\n",
    "<center><b>Figure 1. </b>Example 3-bus power network.</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As shown, the network includes power lines, generation and demand. This information will be modelled in per unit value, so we will need to define an MVA base."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "Elec_Network = {}  # Creating python object\n",
    "Elec_Network['Base'] = 100  # Adding base"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- To represent the network, we can describe the connectivity of each line and the impedances ($Z = R + jX$) across every line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "Elec_Network['Connectivity'] = np.array([[1, 2], [1, 3], [2, 3]])\n",
    "Elec_Network['R'] = [0.05, 0.05, 0.05]  # [pu]\n",
    "Elec_Network['X'] = [0.5, 0.5, 0.5]  # [pu]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- The generators are presented in terms of their active and reactive power injections"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "Elec_Network['Generation_Active'] = [50.8622, 0, 10.173]\n",
    "Elec_Network['Generation_Reactive'] = [27.363, 0, 10.2181]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- The demands are also expressed in terms of their active and reactive components:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "Elec_Network['Demand_Active'] = [0.1451, 30, 30.136]\n",
    "Elec_Network['Demand_Reactive'] = [0, 15, 15]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The aim of the power flow calculation is to determine the outputs of all generators with variable outputs (i.e., slack generators and generators connected to PV buses) which can sustain the power injections introduced by the demands and fixed generators declared above. For the sake of simplicity, let us just declare a slack bus in a selected location:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "Elec_Network['Slack_Bus'] = 1\n",
    "Elec_Network['Slack_Voltage'] = 1.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have all the information required to populate the steady-state equations of the power system, which are presented below:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "P_k = \\sum {V_k V_i (G_{ki} cos \\theta_{ki} + B_{ki} sin \\theta_{ki})}\n",
    "$$\n",
    "\n",
    "$$\n",
    "Q_k = \\sum {V_k V_i (G_{ki} sin \\theta_{ki} + B_{ki} cos \\theta_{ki})}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "where:\n",
    "- $P_k$ is the net active power injection at a node, e.g., for node 1, it would be the active generation minus the active demand.\n",
    "- $Q_k$ is the net reactive power injection at a node, e.g., for node 1, it would be the reactive generation minus the reactive demand.\n",
    "- V and $\\theta$ are the polar components of complex voltages at each node.\n",
    "- G and B are respectively the admittance and susceptance components of the $Y_{bus}$ matrix."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The $Y_{bus}$ matrix will have to be calculated to populate the power flow equations. The matrix can be generated with basic circuit analysis, or by following the shortcut method below:\n",
    "- Convert all line impedances to admittances"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_Admittance(Elec_Network):\n",
    "    Elec_Network['G'] = []\n",
    "    Elec_Network['B'] = []\n",
    "    for i in range(len(Elec_Network['R'])):\n",
    "        val = (Elec_Network['R'][i]**2+Elec_Network['X'][i]**2)**0.5\n",
    "        ang = Elec_Network['R'][i]/val\n",
    "        Elec_Network['G'].append(Elec_Network['R'][i]/val/val)\n",
    "        Elec_Network['B'].append(-Elec_Network['X'][i]/val/val)\n",
    "get_Admittance(Elec_Network)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- The diagonal elements of the $Y_{bus}$ can be calculated as the summation of admittances connected to the node\n",
    "- The off-diagonal elements (representing connections between nodes) can be taken as the negative of the admittance connecting the relevant nodes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 0.3960396-3.96039604j -0.1980198+1.98019802j -0.1980198+1.98019802j]\n",
      " [-0.1980198+1.98019802j  0.3960396-3.96039604j -0.1980198+1.98019802j]\n",
      " [-0.1980198+1.98019802j -0.1980198+1.98019802j  0.3960396-3.96039604j]]\n"
     ]
    }
   ],
   "source": [
    "def get_Ybus(Elec_Network):\n",
    "    '''Method to calculate Ybus'''\n",
    "    Elec_Network['Buses'] = len(Elec_Network['Demand_Active'])\n",
    "    Elec_Network['Lines'] = len(Elec_Network['G'])\n",
    "    \n",
    "    Ybus = np.zeros((Elec_Network['Buses'], Elec_Network['Buses']), dtype=complex)\n",
    "    for b in np.arange(Elec_Network['Lines']):\n",
    "        x = Elec_Network['Connectivity'][b][0]-1\n",
    "        y = Elec_Network['Connectivity'][b][1]-1\n",
    "\n",
    "        # Off-diagonal elements\n",
    "        Ybus[x, y] -= (Elec_Network['G'][b] + Elec_Network['B'][b] * 1j)\n",
    "        Ybus[y, x] -= (Elec_Network['G'][b] + Elec_Network['B'][b] * 1j)\n",
    "\n",
    "        # Diagonal elements\n",
    "        Ybus[x, x] += (Elec_Network['G'][b] + Elec_Network['B'][b] * 1j)\n",
    "        Ybus[y, y] += (Elec_Network['G'][b] + Elec_Network['B'][b] * 1j)\n",
    "    \n",
    "    Elec_Network['Ybus'] = Ybus\n",
    "get_Ybus(Elec_Network)\n",
    "print(Elec_Network['Ybus'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have all the data, to simulate the power system, we need to identify the values of the unknown variables (most V and $\\theta$ values). There are a wide range of models available to solve these equations; we will use one of the most established models, namely Newton's method."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Newton's method applied to power systems"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At a high level, Newton's method is an approach that guesses an initial value iteratively approximates unknown variables in a function so that the function becomes close to zero.\n",
    "\n",
    "For that purpose, an initial value is assigned to the unknown variable ($x_k$), and evaluated by the equation ($f('X_k)$) and its derivative ($f(X_k)$). This information and the equation below are used to estimate an improved value of the unknown variable ($x_{k+1}$) which should take the equation closer to zero."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "x_{k+1} = x_k - \\frac{f(x_k)}{f'(x_k)}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This process is repeated until the function is closed to zero within an acceptable error margin."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on the above, to apply Newton's method, need to:\n",
    "- Identify the unknown variables in the system. These would be most V and $\\theta$ values excluding the slack bus (and voltage magnitudes for PV buses). In our example, the unknown variables would be $V_2$, $V_3$, $\\theta_2$ and $\\theta_3$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Now that we identified the unknown variables, initial guesses for their values can be assigned. Considering the characteristics of the power system, an acceptable guess is to set all voltage magnitues to one and all angles to zero. These are known as flat-start assumptions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Next, we need to set the power flow equations to zero, which can be achieved by substracting the power injections as shown below. This will produce $\\Delta P_k$ and $\\Delta Q_k$ values which show how far the functions are from zero based on our guesses for the unknown variables."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "\\Delta P_k = \\sum {V_k V_i (G_{ki} cos \\theta_{ki} + B_{ki} sin \\theta_{ki})} - P_k\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\Delta Q_k = \\sum {V_k V_i (G_{ki} sin \\theta_{ki} + B_{ki} cos \\theta_{ki})} - Q_k\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Newtons correction factor has to be developed in matrix form, as we have several unknown variables:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "$$\n",
    "x_{k+1} = x_k - J^{-1}(x_k)f(x_k)\n",
    "$$\n",
    "\n",
    "$$\n",
    "x_{k} = \\begin{bmatrix} \\theta_k\\\\V_k\\end{bmatrix}\n",
    "$$\n",
    "\n",
    "$$\n",
    "J = \\begin{bmatrix} \\frac{\\partial \\Delta P_k}{\\partial\\Delta \\theta_k} & \\frac{\\partial \\Delta P_k}{\\partial\\Delta V_k} \\\\ \\frac{\\partial \\Delta Q_k}{\\partial\\Delta \\theta_k} & \\frac{\\partial \\Delta Q_k}{\\partial\\Delta V_k}\\end{bmatrix}\n",
    "$$\n",
    "\n",
    "$$\n",
    "f(x_k) = \\begin{bmatrix} \\Delta P_k\\\\ \\Delta V_k\\end{bmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Coding Newton's method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This section will briefly provide the code required to develop the Jacobian matrix ($J$), vector of differences ($f(x_k)$) and iterative approach required to apply Newton's method to solve the power system problem.\n",
    "\n",
    "For convenience, several parameters are assigned shorter names (e.g., mV instead of Elec_Network['Voltage_Magnitude']) and are stored in polar form."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "x_{k} = \\begin{bmatrix} \\theta_k\\\\V_k\\end{bmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_Polar(Elec_Network):\n",
    "    ''' Get data in polar form '''\n",
    "    \n",
    "    # Number of buses\n",
    "    N = Elec_Network['Buses']\n",
    "    \n",
    "    # Slack\n",
    "    slack = Elec_Network['Slack_Bus']-1\n",
    "    \n",
    "    # Power injections\n",
    "    P = np.ones(Elec_Network['Buses'])\n",
    "    Q = np.ones(Elec_Network['Buses'])\n",
    "    for x in range(Elec_Network['Buses']):\n",
    "        P[x] = (Elec_Network['Generation_Active'][x] -\n",
    "                Elec_Network['Demand_Active'][x])/Elec_Network['Base']\n",
    "        Q[x] = (Elec_Network['Generation_Reactive'][x] -\n",
    "                Elec_Network['Demand_Reactive'][x])/Elec_Network['Base']\n",
    "\n",
    "    # Y bus\n",
    "    aY = np.ones((Elec_Network['Buses'], Elec_Network['Buses']))\n",
    "    mY = np.ones((Elec_Network['Buses'], Elec_Network['Buses']))\n",
    "    for x in range(Elec_Network['Buses']):\n",
    "        for y in range(Elec_Network['Buses']):\n",
    "            aY[x][y] = cmath.phase(Elec_Network['Ybus'][x][y])\n",
    "            mY[x][y] = abs(Elec_Network['Ybus'][x][y])\n",
    "\n",
    "    # Voltages - Assuming flat start\n",
    "    Elec_Network['Voltage_Magnitude'] = np.ones((Elec_Network['Buses']))\n",
    "    Elec_Network['Voltage_Angle'] = np.zeros((Elec_Network['Buses']))\n",
    "\n",
    "    mV = Elec_Network['Voltage_Magnitude']\n",
    "    aV = Elec_Network['Voltage_Angle']\n",
    "    mV[slack] = Elec_Network['Slack_Voltage']\n",
    "    \n",
    "    return (P, Q, aY, mY, mV, aV, N, slack)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Jacobian matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The jacobian matrix is developped by differentiating the $\\Delta P_k$ and $\\Delta Q_k$ equations with respect to the unknown variables ($V$ and $\\thetas$). \n",
    "\n",
    "We will not cover the maths required for this, and instead provide the code directly. That said, there is abundant literature about the topic, such as [this book](https://www.readinglists.manchester.ac.uk/leganto/readinglist/citation/327930681560001631?institute=44MAN_INST&auth=CAS)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "J = \\begin{bmatrix} \\frac{\\partial \\Delta P_k}{\\partial\\Delta \\theta_k} & \\frac{\\partial \\Delta P_k}{\\partial\\Delta V_k} \\\\ \\frac{\\partial \\Delta Q_k}{\\partial\\Delta \\theta_k} & \\frac{\\partial \\Delta Q_k}{\\partial\\Delta V_k}\\end{bmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_Jacobian(Elec_Network, aY, mY, mV, aV, N, slack):\n",
    "    ''' Build Jacobian matrix'''\n",
    "    J = np.zeros((2*(N-1), 2*(N-1)))\n",
    "    \n",
    "    if slack == 0:\n",
    "        x = 1\n",
    "    else:\n",
    "        x = 0\n",
    "    xj = 0\n",
    "    while x < N:\n",
    "        if slack == 0:\n",
    "            y = 1\n",
    "        else:\n",
    "            y = 0\n",
    "        yj = 0\n",
    "        while y < N:\n",
    "            if x==y:\n",
    "                J[xj][xj+N-1] = 2*mV[x]*mY[x][x]*np.cos(aY[x][x])\n",
    "                J[xj+N-1][xj+N-1]=-2*mV[x]*mY[x][x]*np.sin(aY[x][x])\n",
    "                for z in range(N):\n",
    "                   if z != x:\n",
    "                       J[xj][xj] += mV[x]*mV[z]*mY[x][z]*np.sin(aY[x][z]-aV[x]+aV[z])\n",
    "                       J[xj][xj+N-1] += mV[z]*mY[x][z]*np.cos(aY[x][z]-aV[x]+aV[z])\n",
    "                       J[xj+N-1][xj] += mV[x]*mV[z]*mY[x][z]*np.cos(aY[x][z]-aV[x]+aV[z])\n",
    "                       J[xj+N-1][xj+N-1] += -mV[z]*mY[x][z]*np.sin(aY[x][z]-aV[x]+aV[z])\n",
    "            else:\n",
    "                if aY[x][y] != 0:\n",
    "                   J[xj][yj] = -1*mV[x]*mV[y]*mY[x][y]*np.sin(aY[x][y]-aV[x]+aV[y])\n",
    "                   J[xj][yj+N-1] = mV[x]*mY[x][y]*np.cos(aY[x][y]-aV[x]+aV[y])\n",
    "                   J[xj+N-1][yj] = -1*mV[y]*J[x-1][y+N-2]\n",
    "                   J[xj+N-1][yj+N-1] = -1*mV[x]*mY[x][y]*np.sin(aY[x][y]-aV[x]+aV[y])\n",
    "            y += 1\n",
    "            if y == slack:\n",
    "                y += 1\n",
    "            yj += 1\n",
    "        x += 1\n",
    "        if x == slack:\n",
    "            x += 1\n",
    "        xj += 1\n",
    "\n",
    "    return J"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Vector of differences"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The power differences at each node ($f(x_k)$) can be calculated by sunvtracting the power injections to the relevant power flow equations as discussed in the previous section."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "f(x_k) = \\begin{bmatrix} \\Delta P_k\\\\ \\Delta Q_k\\end{bmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_Power_Differences(P, Q, aY, mY, mV, aV, N, slack):\n",
    "    ''' # Get vector of differences '''\n",
    "    df = np.zeros(2*(N-1))\n",
    "    if slack == 0:\n",
    "        x = 1\n",
    "    else:\n",
    "        x = 0\n",
    "    xj = 0\n",
    "    while x < N:\n",
    "        df[xj] = P[x]-mV[x]**2*mY[x][x]*np.cos(aY[x][x])\n",
    "        df[xj+N-1] = Q[x]+mV[x]**2*mY[x][x]*np.sin(aY[x][x])\n",
    "        y = 0\n",
    "        while y < N:\n",
    "            if x != y:\n",
    "                df[xj] += -mV[x]*mV[y]*mY[x][y]*np.cos(aY[x][y]-aV[x]+aV[y])\n",
    "                df[xj+N-1] += mV[x]*mV[y]*mY[x][y]*np.sin(aY[x][y]-aV[x]+aV[y])\n",
    "            y += 1\n",
    "        x += 1\n",
    "        if x == slack:\n",
    "            x += 1\n",
    "        xj += 1\n",
    "    \n",
    "    return df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Correction factors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now calculate the correction factors and use them to update the unknown variables ($V$ and $\\theta$)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "x_{k+1} = x_k - J^{-1}(x_k)f(x_k)\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "def update_Voltages(mV, aV, J, df, N, slack):\n",
    "    ''' Update volteges with correction factors '''\n",
    "\n",
    "    # Get correction factors and update unknown variables\n",
    "    dx = np.linalg.inv(J).dot(df)\n",
    "    \n",
    "    # Update unknown variables\n",
    "    x = 0\n",
    "    for Node in np.arange(N):\n",
    "        if Node != slack:\n",
    "            aV[Node] += dx[x]\n",
    "            mV[Node] += dx[x + N - 1]\n",
    "            x += 1\n",
    "    return dx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solving the model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now bring together all the methods we have created above and code Newton's method. The processes that are only needed once (e.g., calculation of $Y_{bus}$ are placed first. The processes that should be applied iteratively (e.g., calculation of Jacobian matrix) are placed within a loop which is repeated until the expected error falls below a threshold."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Voltage magnitude: [1.         0.91313517 0.93469457] [pu]\n",
      "Voltage Angle:  [ 0.         -0.13944711 -0.121194  ] [rad]\n",
      "Iterations:  4\n"
     ]
    }
   ],
   "source": [
    "def Newton_Elec(Elec_Network):\n",
    "    # Build Y bus\n",
    "    get_Admittance(Elec_Network)\n",
    "    get_Ybus(Elec_Network)\n",
    "    \n",
    "    # Get parameters - shorter names   \n",
    "    (P, Q, aY, mY, mV, aV, N, slack)=get_Polar(Elec_Network)    \n",
    "    \n",
    "    dx = np.inf  # Current error\n",
    "    Max_it = 20  # Maximum number of iterations\n",
    "    Elec_Network['Iteration'] = 0  # Current iteration\n",
    "    Elec_Network['Succes'] = True  # Flag for convergence\n",
    "    while np.max(np.abs(dx)) > 1e-6:\n",
    "        Elec_Network['Iteration'] += 1\n",
    "        \n",
    "        # Get Jacobian matrix\n",
    "        J = get_Jacobian(Elec_Network, aY, mY, mV, aV, N, slack)\n",
    "        \n",
    "        # Get vector of differences\n",
    "        df = get_Power_Differences(P, Q, aY, mY, mV, aV, N, slack)\n",
    "        \n",
    "        # Get correction factors and update unknown variables\n",
    "        dx = update_Voltages(mV, aV, J, df, N, slack)\n",
    "\n",
    "        if Elec_Network['Iteration'] >= Max_it:\n",
    "            Elec_Network['Succes'] = False\n",
    "            dx = 0\n",
    "            print('The model did not converge after %d iterations'%Max_it)    \n",
    "           \n",
    "Newton_Elec(Elec_Network)\n",
    "print('Voltage magnitude:', Elec_Network['Voltage_Magnitude'], '[pu]')\n",
    "print('Voltage Angle: ', Elec_Network['Voltage_Angle'], '[rad]')\n",
    "print('Iterations: ', Elec_Network['Iteration'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Development as python tool"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code avobe provides the means to solve the power flow equations using Newton's method, but is functionalities as a tool are limited. For starters, the model only provides the voltages for every bus across the network, whereas there are other parameters that may be of interest for us, such as the currents, power losses, etc.\n",
    "\n",
    "Accordingly, it is convenient to add the calculations of the different parameters to our model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_Parameters(Elec_Network):\n",
    "    ''' Calculate additional parameters '''\n",
    "    Elec_Network['Current'] = []\n",
    "    Elec_Network['Sending_Power'] = []\n",
    "    Elec_Network['Receiving_Power'] = []\n",
    "    Elec_Network['Loss'] = []\n",
    "    for b in range(Elec_Network['Lines']):\n",
    "        s = Elec_Network['Connectivity'][b][0]-1  # Supply side\n",
    "        r = Elec_Network['Connectivity'][b][1]-1  # Sending side\n",
    "        Y = complex(Elec_Network['G'][b], Elec_Network['B'][b])\n",
    "        Vs = Elec_Network['Voltage_Magnitude'][s] * \\\n",
    "            complex(cmath.cos(Elec_Network['Voltage_Angle'][s]),\n",
    "                    cmath.sin(Elec_Network['Voltage_Angle'][s]))\n",
    "        Vr = Elec_Network['Voltage_Magnitude'][r] * \\\n",
    "            complex(cmath.cos(Elec_Network['Voltage_Angle'][r]),\n",
    "                    cmath.sin(Elec_Network['Voltage_Angle'][r]))\n",
    "        I = Y * (Vs - Vr)\n",
    "        Ss = Vs*I.conjugate()*Elec_Network['Base']\n",
    "        Sr = -Vr*I.conjugate()*Elec_Network['Base']\n",
    "\n",
    "        Elec_Network['Current'].append(I)\n",
    "        Elec_Network['Sending_Power'].append(Ss)\n",
    "        Elec_Network['Receiving_Power'].append(Sr)\n",
    "        Elec_Network['Loss'].append(Ss+Sr)\n",
    "get_Parameters(Elec_Network)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can take this further by adding functionalities to the model to display all findings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "VOLTAGES  [pu] [deg]:\n",
      " 1)   1.0000 +j   0.0000 (  1.0000 ∠   0.0000)\n",
      " 2)   0.9043 +j  -0.1269 (  0.9131 ∠  -7.9897)\n",
      " 3)   0.9278 +j  -0.1130 (  0.9347 ∠  -6.9439)\n",
      "CURRENTS [pu] [deg]:\n",
      " 1- 2)   0.2703 +j  -0.1644 (  0.3164 ∠ -31.3142)\n",
      " 1- 3)   0.2381 +j  -0.1205 (  0.2668 ∠ -26.8511)\n",
      " 2- 3)  -0.0322 +j   0.0439 (  0.0545 ∠ 126.2780)\n",
      "POWER  [MVA]:\n",
      "      From:                To:                   Loss:\n",
      " 1- 2)  27.0286 +j  16.4429 -26.5282 +j -11.4383 (  0.5005 +j   5.0046)\n",
      " 1- 3)  23.8056 +j  12.0517 -23.4496 +j  -8.4920 (  0.3560 +j   3.5598)\n",
      " 2- 3)  -3.4718 +j  -3.5617   3.4866 +j   3.7101 (  0.0148 +j   0.1483)\n"
     ]
    }
   ],
   "source": [
    "def Visualize_Elec(Elec_Network, flg=[True, True, True]):\n",
    "    if flg[0] and Elec_Network['Succes']:\n",
    "        print('VOLTAGES  [pu] [deg]:')\n",
    "        for n in range(Elec_Network['Buses']):\n",
    "            V = Elec_Network['Voltage_Magnitude'][n] * \\\n",
    "                complex(cmath.cos(Elec_Network['Voltage_Angle'][n]),\n",
    "                        cmath.sin(Elec_Network['Voltage_Angle'][n]))\n",
    "            print('%2.0f) %8.4f +j %8.4f (%8.4f ∠ %8.4f)'\n",
    "                  %(n+1, V.real, V.imag, abs(V), cmath.phase(V)*180/math.pi))\n",
    "    if flg[1] and Elec_Network['Succes']:\n",
    "        print('CURRENTS [pu] [deg]:')\n",
    "        for b in range(Elec_Network['Lines']):\n",
    "            s = Elec_Network['Connectivity'][b][0]\n",
    "            r = Elec_Network['Connectivity'][b][1]\n",
    "            I = Elec_Network['Current'][b]\n",
    "            print('%2.0f-%2.0f) %8.4f +j %8.4f (%8.4f ∠ %8.4f)'\n",
    "                  %(s, r, I.real, I.imag, abs(I), cmath.phase(I)*180/math.pi))\n",
    "    if flg[2] and Elec_Network['Succes']:\n",
    "        print('POWER  [MVA]:')\n",
    "        print('      From:                To:                   Loss:')\n",
    "        for b in range(Elec_Network['Lines']):\n",
    "            s = Elec_Network['Connectivity'][b][0]\n",
    "            r = Elec_Network['Connectivity'][b][1]\n",
    "            Ss = Elec_Network['Sending_Power'][b]\n",
    "            Sr = Elec_Network['Receiving_Power'][b]\n",
    "            print('%2.0f-%2.0f) %8.4f +j %8.4f %8.4f +j %8.4f (%8.4f +j %8.4f)'\n",
    "                  %(s, r, Ss.real, Ss.imag, Sr.real, Sr.imag, Ss.real+Sr.real,Ss.imag+Sr.imag))\n",
    "\n",
    "Visualize_Elec(Elec_Network)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above is useful, but we would need to use several methods to access these functionalities. \n",
    "\n",
    ">It would be more convenient to have all these functionalities within the same model. \n",
    "\n",
    "This can be achieved by puting the models within a python class as follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Elec_Model:\n",
    "    import numpy as np\n",
    "    import scipy.sparse as sp\n",
    "    import cmath, math\n",
    "\n",
    "    def __init__(self, obj=None):\n",
    "        '''Default values '''\n",
    "        self.parameters = {}\n",
    "        self.parameters['Base'] = 100\n",
    "        self.parameters['Slack_Voltage'] = 1.0\n",
    "        self.parameters['Slack_Bus'] = 1\n",
    "        \n",
    "        if obj is not None:\n",
    "            for pars in obj.keys():\n",
    "                self.parameters[pars] = obj[pars]\n",
    "\n",
    "    def run(self):\n",
    "        Newton_Elec(self.parameters)\n",
    "        get_Parameters(self.parameters)\n",
    "\n",
    "    def display(self):\n",
    "        Visualize_Elec(self.parameters)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This object, can take our Elec_Model as inputs, and automatically declares some default input data. For example, the MVA base and slack voltage magnitude are not included, so the model automatically adds them. It can also be seen that when a parameter is declared it overrides its default value, e.g., the slack bus is node 3 instead of 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'Base': 100, 'Slack_Voltage': 1.0, 'Slack_Bus': 3, 'Connectivity': array([[1, 2],\n",
      "       [1, 3],\n",
      "       [2, 3]]), 'R': [0.05, 0.05, 0.05], 'X': [0.5, 0.5, 0.5], 'Generation_Active': [50.8622, 0, 10.173], 'Generation_Reactive': [27.363, 0, 10.2181], 'Demand_Active': [0.1451, 30, 30.136], 'Demand_Reactive': [0, 15, 15]}\n"
     ]
    }
   ],
   "source": [
    "# Defining input data\n",
    "Elec_Network = {}\n",
    "Elec_Network['Connectivity'] = np.array([[1, 2], [1, 3], [2, 3]])\n",
    "Elec_Network['R'] = [0.05, 0.05, 0.05]  # [pu]\n",
    "Elec_Network['X'] = [0.5, 0.5, 0.5]  # [pu]\n",
    "\n",
    "Elec_Network['Generation_Active'] = [50.8622, 0, 10.173]\n",
    "Elec_Network['Generation_Reactive'] = [27.363, 0, 10.2181]\n",
    "\n",
    "Elec_Network['Demand_Active'] = [0.1451, 30, 30.136]\n",
    "Elec_Network['Demand_Reactive'] = [0, 15, 15]\n",
    "\n",
    "Elec_Network['Slack_Bus'] = 3 \n",
    "\n",
    "# Initialising model\n",
    "model = Elec_Model(Elec_Network)\n",
    "\n",
    "# Displaying data taken my the model\n",
    "print(model.parameters)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model can solve the power system model with its `run` method, and show the results with its `display` method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "VOLTAGES  [pu] [deg]:\n",
      " 1)   1.0540 +j   0.1129 (  1.0600 ∠   6.1148)\n",
      " 2)   0.9799 +j  -0.0155 (  0.9800 ∠  -0.9067)\n",
      " 3)   1.0000 +j   0.0000 (  1.0000 ∠   0.0000)\n",
      "CURRENTS [pu] [deg]:\n",
      " 1- 2)   0.2690 +j  -0.1212 (  0.2950 ∠ -24.2565)\n",
      " 1- 3)   0.2343 +j  -0.0845 (  0.2491 ∠ -19.8360)\n",
      " 2- 3)  -0.0347 +j   0.0367 (  0.0505 ∠ 133.3962)\n",
      "POWER  [MVA]:\n",
      "      From:                To:                   Loss:\n",
      " 1- 2)  26.9793 +j  15.8105 -26.5442 +j -11.4591 (  0.4351 +j   4.3514)\n",
      " 1- 3)  23.7378 +j  11.5525 -23.4276 +j  -8.4511 (  0.3101 +j   3.1014)\n",
      " 2- 3)  -3.4558 +j  -3.5409   3.4686 +j   3.6684 (  0.0127 +j   0.1274)\n"
     ]
    }
   ],
   "source": [
    "model.run()\n",
    "model.display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "165px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
