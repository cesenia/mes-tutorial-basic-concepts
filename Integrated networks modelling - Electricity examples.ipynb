{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Integrated networks modelling - Electricity examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***&copy; 2023 Martínez Ceseña<sup>1</sup> and Mancarella<sup>2</sup> — <sup>1,2</sup>University of Manchester, UK, and <sup>2</sup>University of Melbourne, Australia***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is one of the documents in a series of jupyter notebooks which presents a Newton's based formulation to simulate integrated electricity, heating and gas networks under steady-state conditions. This particular notebook presents several case studies for the application of the power system model. More information about the models and some of their applications can also be found in the following literature:\n",
    "\n",
    "1. X. Liu and P. Mancarella, \"[Modelling, assessment and Sankey diagrams of integrated electricity-heat-gas networks in multi-vector district energy systems](https://www.sciencedirect.com/science/article/pii/S0306261915010259)\" in Applied Energy, Vol. 167, pp. 136 - 352, 2016.\n",
    "\n",
    "1. E. A. Martínez Ceseña, E. Loukarakis, N. Good and P. Mancarella, \"[Integrated Electricity-Heat-Gas Systems: Techno-Economic Modeling, Optimization, and Application to Multienergy Districts](https://ieeexplore.ieee.org/document/9108286),\" in Proceedings of the IEEE, Vol. 108, pp. 1392 –1410, 2020.\n",
    "\n",
    "1. E. A. Martínez Ceseña, T. Capuder and P. Mancarella, “[Flexible distributed multi-energy generation system expansion planning under uncertainty](https://ieeexplore.ieee.org/document/7064771),” IEEE Transactions on Smart Grid, Vol. 7, pp. 348 –357, 2016.\n",
    "\n",
    "1. G. Chicco, S. Riaz, A. Mazza and P. Mancarella, \"[Flexibility From Distributed Multienergy Systems](https://ieeexplore.ieee.org/document/9082595),\" in Proceedings of the IEEE, Vol. 108, pp. 1496-1517, 2020.\n",
    "\n",
    "1. E. Corsetti, S. Riaz, M. Riello, P. Mancarella, “[Modelling and deploying multi-energy flexibility: The energy lattice framework](https://www.sciencedirect.com/science/article/pii/S2666792421000238)”, Advances in Applied Energy, Vol. 2, 2021.\n",
    "\n",
    "Other notebooks that cover the formulation of the integrated network model include:\n",
    "- *Integrated networks modelling - Heat examples*\n",
    "- *Integrated networks modelling - Gas examples*\n",
    "- *Integrated networks modelling - Examples*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## List of contents"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Same configuration but different values](#Same-configuration-but-different-values)\n",
    "- [Different system configuration](#Different-system-configuration)\n",
    "- [Interactive system](#Interactive-system)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity-examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Before we begin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we begin, be aware that, to benefit the most from this notebook, you will need a basic understanding of: \n",
    "- [Newton's method](https://www.sciencedirect.com/topics/mathematics/newtons-method), which is the method used in this notebook.\n",
    "- [Python](https://www.python.org/), which is the language used in this notebook.\n",
    "\n",
    "It is also strongly suggested to review the following notebook:\n",
    "- Integrated networks modelling - Electricity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The notebook requires some python functionalities, which should be imported as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import ipywidgets as widgets\n",
    "from ipywidgets import interact"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The notebook also requires a power network simulator developed in another notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nbimporter\n",
    "Power_Network = __import__('Integrated networks modelling - Electricity')\n",
    "Elec_Model = Power_Network.Elec_Model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity-examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have loaded all the required tools, let us simulate a few power systems under different conditions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Same configuration but different values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can begin with applications of the model to the same system after changing some parameters. The default system settings lead to low voltage problems. \n",
    "\n",
    "***How can you solve the issue?***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Power_Network_3Bus.png](Figures/Power_Network_3Bus.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "VOLTAGES  [pu] [deg]:\n",
      " 1)   0.9330 +j  -0.1585 (  0.9463 ∠  -9.6392)\n",
      " 2)   0.9174 +j  -0.1809 (  0.9350 ∠ -11.1534)\n",
      " 3)   1.0000 +j   0.0000 (  1.0000 ∠   0.0000)\n",
      "CURRENTS [pu] [deg]:\n",
      " 1- 2)   0.1187 +j  -0.0660 (  0.1358 ∠ -29.0753)\n",
      " 1- 3)  -0.3270 +j   0.1014 (  0.3424 ∠ 162.7771)\n",
      " 2- 3)  -0.2341 +j   0.0799 (  0.2473 ∠ 161.1615)\n",
      "POWER  [MVA]:\n",
      "      From:                To:                   Loss:\n",
      " 1- 2)  12.1186 +j   4.2762 -12.0817 +j  -3.9074 (  0.0369 +j   0.3688)\n",
      " 1- 3) -32.1186 +j  -4.2762  32.7048 +j  10.1381 (  0.5862 +j   5.8619)\n",
      " 2- 3) -22.9183 +j  -3.0926  23.4076 +j   7.9862 (  0.4894 +j   4.8936)\n"
     ]
    }
   ],
   "source": [
    "Elec_Network = {}\n",
    "Elec_Network['Connectivity'] = np.array([[1, 2], [1, 3], [2, 3]])\n",
    "Elec_Network['R'] = [0.02, 0.05, 0.08]\n",
    "Elec_Network['X'] = [0.2, 0.5, 0.8]\n",
    "\n",
    "Elec_Network['Demand_Active'] = [20, 35, 30]\n",
    "Elec_Network['Demand_Reactive'] = [0, 7, 15]\n",
    "Elec_Network['Generation_Active'] = [0, 0, 0]\n",
    "Elec_Network['Generation_Reactive'] = [0, 0, 0]\n",
    "\n",
    "Elec_Network['Slack_Bus'] = 3 \n",
    "\n",
    "# Simulate network using Newton's method\n",
    "model = Elec_Model(Elec_Network)\n",
    "model.run()\n",
    "model.display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity-examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Different system configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible to create other system configurations as shown below. There are once again voltage problems. \n",
    "\n",
    "***Assuming you can constrol `Generator 1` and `Geberator 5`, how can you solve the problem?***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Power_Network_5Bus.png](Figures/Power_Network_5Bus.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "VOLTAGES  [pu] [deg]:\n",
      " 1)   1.0000 +j   0.0000 (  1.0000 ∠   0.0000)\n",
      " 2)   0.9501 +j  -0.0570 (  0.9518 ∠  -3.4334)\n",
      " 3)   0.9070 +j  -0.1044 (  0.9130 ∠  -6.5670)\n",
      " 4)   0.8639 +j  -0.1518 (  0.8772 ∠  -9.9673)\n",
      " 5)   0.9070 +j  -0.1044 (  0.9130 ∠  -6.5670)\n",
      "CURRENTS [pu] [deg]:\n",
      " 1- 2)   0.0656 +j  -0.0172 (  0.0678 ∠ -14.6623)\n",
      " 2- 3)   0.0552 +j  -0.0155 (  0.0573 ∠ -15.6779)\n",
      " 3- 4)   0.0552 +j  -0.0155 (  0.0573 ∠ -15.6779)\n",
      " 3- 5)   0.0000 +j  -0.0000 (  0.0000 ∠ -70.5600)\n",
      "POWER  [MVA]:\n",
      "      From:                To:                   Loss:\n",
      " 1- 2)   6.5579 +j   1.7158  -6.3282 +j  -1.2563 (  0.2298 +j   0.4595)\n",
      " 2- 3)   5.3282 +j   1.1563  -5.1641 +j  -0.8282 (  0.1641 +j   0.3282)\n",
      " 3- 4)   5.1641 +j   0.8282  -5.0000 +j  -0.5000 (  0.1641 +j   0.3282)\n",
      " 3- 5)   0.0000 +j   0.0000  -0.0000 +j  -0.0000 (  0.0000 +j   0.0000)\n"
     ]
    }
   ],
   "source": [
    "Elec_Network = {}\n",
    "Elec_Network['Connectivity'] = np.array([[1, 2], [2, 3], [3, 4], [3, 5]])\n",
    "Elec_Network['R'] = [0.5, 0.5, 0.5, 0.2]\n",
    "Elec_Network['X'] = [1.0, 1.0, 1.0, 0.4]\n",
    "\n",
    "Elec_Network['Demand_Active'] = [0, 1, 0, 5, 0]\n",
    "Elec_Network['Demand_Reactive'] = [0, 0.1, 0, 0.5, 0]\n",
    "Elec_Network['Generation_Active'] = [0, 0, 0, 0, 0]\n",
    "Elec_Network['Generation_Reactive'] = [0, 0, 0, 0, 0]\n",
    "\n",
    "Elec_Network['Slack_Voltage'] = 1.0\n",
    "\n",
    "# Simulate network using Newton's method\n",
    "model = Elec_Model(Elec_Network)\n",
    "model.run()\n",
    "model.display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity-examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interactive system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets use the same system, but this time you can only control the output of `Geberator 5`.\n",
    "\n",
    "*** how can you solve the problem?***\n",
    "\n",
    "> Use the sliders"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Power_Network_5Bus.png](Figures/Power_Network_5Bus.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "82711b6a1e0f4bfc90b2693438159eb7",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.0, description='P5', max=1.0, step=0.01), FloatSlider(value=0.0, des…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Pow_Sys(P5 = widgets.FloatSlider(min=0,max=1,step=0.01,value=0.0), \n",
    "            Q5 = widgets.FloatSlider(min=0,max=1,step=0.01,value=0.0)):\n",
    "    Elec_Network = {}\n",
    "    Elec_Network['Connectivity'] = np.array([[1, 2], [2, 3], [3, 4], [3, 5]])\n",
    "    Elec_Network['R'] = [0.5, 0.5, 0.5, 0.2]\n",
    "    Elec_Network['X'] = [1.0, 1.0, 1.0, 0.4]\n",
    "\n",
    "    Elec_Network['Demand_Active'] = [0, 1, 0, 5, 0]\n",
    "    Elec_Network['Demand_Reactive'] = [0, 0.1, 0, 0.5, 0]\n",
    "    Elec_Network['Generation_Active'] = [0, 0, 0, 0, P5]\n",
    "    Elec_Network['Generation_Reactive'] = [0, 0, 0, 0, Q5]\n",
    "\n",
    "    Elec_Network['Slack_Voltage'] = 1.05\n",
    "\n",
    "    # Simulate network using Newton's method\n",
    "    model = Elec_Model(Elec_Network)\n",
    "    model.run()\n",
    "    model.display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#Integrated-networks-modelling---Electricity-examples)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "165px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
